FROM maven:3.6.1-jdk-11-slim

# Let's start with some basic stuff.
RUN apt-get update -qq && apt-get install -qqy \
  apt-transport-https sudo \
  ca-certificates \
  curl \
  git 

RUN export uid=1000 gid=1000 && \
  mkdir -p /home/aoki && \
  echo "aoki:x:${uid}:${gid}:Developer,,,:/home/aoki:/bin/bash" >> /etc/passwd && \
  echo "aoki:x:${uid}:" >> /etc/group && \
  echo "aoki ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/aoki && \
  chmod 0440 /etc/sudoers.d/aoki && \
  chown ${uid}:${gid} -R /home/aoki

USER aoki
ENV HOME /home/aoki

#ENTRYPOINT [ "mvn" ]
CMD /bin/bash
